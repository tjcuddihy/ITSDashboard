# -*- coding: utf-8 -*-
"""
Created on Sun Feb 15 16:49:46 2015

@author: tom
"""

import json
import urllib.request
from datetime import datetime
from bs4 import BeautifulSoup

#==============================================================================
# Open dashboard template and create soup for LIVE creation.
#==============================================================================
f = open('/home/pi/ITSDashboard/dashboard.html','r')
html_text = f.read()
f.close()
soup = BeautifulSoup(html_text)

#==============================================================================
# Find and store shipping details
#==============================================================================

# Try to load the shipping website, if it's unavailable then put "Error, website down" Into shipping table
try:
	shipping = urllib.request.urlopen('http://shipschedule.sydneyports.com.au/marineoperations/movements.asp').read()
	ships = BeautifulSoup(shipping).body
	soup_botany = ships.find_all('table')[2].find_all("tr") #index for Port Botany arrivals 
	# In soup_botany, you can now loop through the rows of the arrivals table. Index 0 and 1 are 		headings, 2 onwards are ships
	# eg soup_botany[2] is the first ship

	soup_ships = soup_botany[2:] #subset out the table headings

	# Loop for finding ships at BLB1 or 2 and then putting the results into the HTML file
	ship_time = []
	ship_berth = []
	ship_name = []
	ship_type = []

	# Find and store ship details if they are heading to BLB 1 or 2
	for ship in soup_ships:
    		if ship.find_all('td')[2].string == "BULK LIQUID BERTH 2(BLB2)":
        		ship_time.append(ship.find_all('td')[1].string)
        		ship_berth.append('BLB2')
        		ship_name.append(ship.find_all('td')[3].string)
        		ship_type.append(ship.find_all('td')[4].string)

    		if ship.find_all('td')[2].string == "BULK LIQUID BERTH 1(BLB1)":
       			ship_time.append(ship.find_all('td')[1].string)
       			ship_berth.append('BLB1')
        		ship_name.append(ship.find_all('td')[3].string)
        		ship_type.append(ship.find_all('td')[4].string)
except:
	ship_time = ['-']*7
	ship_berth = ['-']*7
	ship_name = ['error, website down']*7
	ship_type = ['-']*7



#==============================================================================
# Start putting ship details into dashboard soup
#==============================================================================

# Loop to go through soup shipping-table rows and add ship data as apporipriate
j=0
for i in range(1,8): # iter for table rows in shipping-table on Dashboard-v1

    if j<len(ship_name): # for making blank rows once all ships are iterated through
        #code for assigning ship details
        soup.find(id="shipping-table").find_all('tr')[i].find_all('td')[0].string = ship_name[j]
        soup.find(id="shipping-table").find_all('tr')[i].find_all('td')[1].string = ship_type[j]
        soup.find(id="shipping-table").find_all('tr')[i].find_all('td')[2].string = ship_time[j]
        soup.find(id="shipping-table").find_all('tr')[i].find_all('td')[3].string = ship_berth[j]
        j = j+1
    else:
        #code for leaving extra ship rows blank
        soup.find(id="shipping-table").find_all('tr')[i].find_all('td')[0].string = ""
        soup.find(id="shipping-table").find_all('tr')[i].find_all('td')[1].string = ""
        soup.find(id="shipping-table").find_all('tr')[i].find_all('td')[2].string = ""
        soup.find(id="shipping-table").find_all('tr')[i].find_all('td')[3].string = ""
        j=j+1


#==============================================================================
# Barometic Pressure related
#==============================================================================

#Try the Bureau Website, if fail get previous results from text file.
try:
	json_weather = urllib.request.urlopen('http://www.bom.gov.au/fwo/IDN60901/IDN60901.94767.json').read().decode("utf-8") # 94767
	data = json.loads(json_weather)
	data2= data.get('observations').get('data')

	website_down = False

	# Pull the Pressure data
	for item in data2:
		if item['press_msl'] is not None: # Checks for the first available valid pressure reading
			pressure_msl=item['press_msl']
			pressure_time= datetime.strptime(str(item['local_date_time_full']), "%Y%m%d%H%M%S") # retrieves and converts time associated with pressure value above
			break
		else: # Catches if there are no pressure values avaiable
			pressure_missing = True

except:
	website_down = True

# If BOM has no pressure values get most recent from pressure_text, if they do assign to Dashboard-v1
if website_down == True:
	pressure_text = open('/home/pi/ITSDashboard/pressure.txt','r')
	pressure_old = pressure_text.read().splitlines()
	pressure_text.close()
	pressure_msl = float(pressure_old[0])
	pressure_time = datetime.strptime(str(pressure_old[1]), "%Y-%m-%d %H:%M:%S")
else: # If all is good, update the pressure_text file with the current pressure time/value for use if there is an error the next time the script runs
	pressure_text = open('/home/pi/ITSDashboard/pressure.txt','w')
	pressure_text.write("%s\n%s" % (pressure_msl,pressure_time))
	pressure_text.close()
	

time_since_pressure_update = datetime.now() - pressure_time
pressure_hours = round((time_since_pressure_update.total_seconds()/3600),1)
pressure_correction = round(0.25*((1013.25-pressure_msl)/10),2)
    
soup.find(id="pressure_value").span.string = "Pressure (kPa) = %s" % pressure_msl
    
if pressure_correction > 0:
	soup.find(id="pressure_correction").span.string = "Correction (℃) = +%s" % pressure_correction
else:
        soup.find(id="pressure_correction").span.string = "Correction (℃) = %s" % pressure_correction
        
soup.find(id="pressure_age").span.string = "Pressure value is %s hours old." % pressure_hours


#==============================================================================
# Create dashboard-LIVE from soup
#==============================================================================
f = open('/home/pi/ITSDashboard/dashboard-LIVE.html','wb')
f.write(soup.prettify().encode("utf-8"))
f.close()
